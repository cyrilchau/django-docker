from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as DjUserAdmin
from django.contrib.auth.forms import UserChangeForm
from django.utils.translation import gettext_lazy as _


class CustomUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = get_user_model()


class UserAdmin(DjUserAdmin):
    form = CustomUserChangeForm
    list_display = (
        'id',
        'username',
        'email',
    )
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (
            _('Personal info'), {
                'fields': (
                    'name',
                    'email',
                    'phone_number',
                ),
            },
        ),
        (
            _('Permissions'), {
                'fields': (
                    'is_active',
                    'is_staff',
                    'is_superuser',
                    'groups',
                    'user_permissions',
                ),
            },
        ),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    ordering = ('email',)
    search_fields = ('username', 'email')
    add_fieldsets = (
        (
            None, {
                'classes': ('wide',),
                'fields': (
                    'username',
                    'email',
                    'password1',
                    'password2',
                ),
            },
        ),
    )
