from django.contrib import admin
from accounts.models import User
from accounts.models import Book

from accounts.admin.user_admin import UserAdmin
from accounts.admin.book_admin import BookAdmin

admin.site.register(User, UserAdmin)
admin.site.register(Book, BookAdmin)
