from django.urls import path
from rest_framework import routers

from accounts.api.user import UserViewSet
from accounts.api.book import BookViewSet
from . import views


router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'books', BookViewSet)


urlpatterns = [
    path('', views.index, name='index'),
]

urlpatterns += router.urls
