from django.core.management.base import BaseCommand, CommandError
from accounts.models import Book, Author
from faker import Faker
import random

fake = Faker()


class Command(BaseCommand):
    def handle(self, *args, **options):
        authors = Author.objects.count()
        if authors < 10:
            for u in range(1, 10):
                Author.objects.create(name=fake.name())

        min_id = Author.objects.first().id
        max_id = Author.objects.last().id
        books = Book.objects.count()
        data = []
        if books < 100:
            for i in range(1, 101):
                random_id = random.randint(min_id, max_id)
                data.append(Book(title=fake.name(), author_id=random_id))

        Book.objects.bulk_create(data)
