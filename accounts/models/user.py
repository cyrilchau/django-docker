from django.db import models

from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import UserManager
from django.db.models.manager import BaseManager


class UserQuerySet(models.QuerySet):
    pass


class CustomUserManager(BaseManager.from_queryset(UserQuerySet), UserManager):
    pass


class User(AbstractUser):
    name = models.CharField(max_length=80, blank=True, null=True)
    email = models.EmailField(null=True, blank=True, unique=True)
    phone_number = models.CharField(max_length=50, unique=True, null=True)
    objects = CustomUserManager()

    def __str__(self):
        return f'{self.name} | {self.phone_number}'



