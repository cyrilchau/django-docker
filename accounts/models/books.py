from django.db import models

from accounts.models import Author
# Create your models here.


class Book(models.Model):
    title = models.CharField(max_length=100)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
