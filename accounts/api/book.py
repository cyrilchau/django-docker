from accounts.models.user import User
from accounts.admin.book_admin import BookAdmin
from accounts.models import Book
from accounts.models import User

from rest_framework import mixins
from rest_framework import serializers
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework import status


class BookSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Book
        fields = ('title',)


class BookViewSet(ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer

    # def list(self, request, *args, **kwargs):
    #     author = User.objects.first()
    #     # queryset = Book.objects.filter(author=author)
    #     # queryset = Book.objects.filter(author=author).select_related('author')
    #     # queryset = Book.objects.filter(author=author).prefetch_related('author')

    #     queryset = Book.objects.filter(author=author)
    #     author_ids = [x.author_id for x in queryset]
    #     authors = User.objects.filter(id__in=author_ids)
    #     author_by_id = {x.id: x for x in authors}
    #     for i in queryset:
    #         i.author = author_by_id[i.author_id]
    #         print(i.author.name)

    #     serializer = self.get_serializer(queryset, many=True)
    #     return Response(serializer.data)
