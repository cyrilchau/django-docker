#!/usr/bin/env bash

main() {
    config
}

config() {
    exec celery -A root worker -l info
}

main "$@"
